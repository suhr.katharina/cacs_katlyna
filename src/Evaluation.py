import logging
logging.getLogger().setLevel(level=logging.INFO)

output = open('outputs\output_stanza.txt', encoding='utf-8')
dev = open("data\EsEn_NER\calcs_dev.tsv", encoding="utf-8")

outputdata = output.readlines()
devdata = dev.readlines()

count = 0
count_right_predicted = 0

for line_output, line_dev in zip(outputdata, devdata):
    
    line_output = line_output.split("\t")
    line_dev = line_dev.split("\t")

    if "O\n" not in line_dev[5]:
        line_output.append(line_dev[5])
        print(line_output)
        count += 1
        if ("S-ORG" in line_output[3] or "S-ORG" in line_output[5]) and "B-ORG" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("B-PERSON" in line_output[3] or "S-PERSON" in line_output[3] or "B-PER" in line_output[5] or "S-PER" in line_output[5]) and "B-PER" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("E-PERSON" in line_output[3] or "E-PER" in line_output[5]) and "I-PER" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("S-GPE" in line_output[3] or "S-LOC" in line_output[5]) and "B-LOC" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("S-PRODUCT" in line_output[3] or "S-PRODUCT" in line_output[5]) and "B-PROD" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("B-WORK_OF_ART" in line_output[3] or "B-WORK_OF_ART" in line_output[5]) and "B-TITLE" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("I-WORK_OF_ART" in line_output[3] or "E-WORK_OF_ART" in line_output[5]) and "I-TITLE" in line_output[6]:
            print(line_output)
            count_right_predicted += 1
        if ("B-MISC" in line_output[3] or "B-MISC" in line_output[5]) and "B-OTHER" in line_output[6]:
            print(line_output)
            count_right_predicted += 1    

print(count)
print(count_right_predicted)

print (float(count_right_predicted) / float(count))