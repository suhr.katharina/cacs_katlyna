from langdetect import detect
from langdetect import detect_langs


doc = "Good Morning."
doc2 = "bottle"
doc3 = "plant"
doc4 = "chair"
doc5 = "reading"
doc6 = "power"


esp = "Buenos días."
esp2 = "botella"
esp3 = "planta"
esp4 = "silla"
esp5 = "leyendo"
esp6 = "poder"

print(detect_langs(doc), detect_langs(doc2), detect_langs(doc3), detect_langs(doc4), detect_langs(doc5), detect_langs(doc6))
print(detect_langs(esp), detect_langs(esp2), detect_langs(esp3), detect_langs(esp4), detect_langs(esp5), detect_langs(esp6))