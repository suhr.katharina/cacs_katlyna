import stanza
import logging
logging.getLogger().setLevel(level=logging.INFO)


class StanzaNER():

    def __init__(self, inputfile, outputfile):
        self.input = inputfile
        self.output = outputfile
        self.generate_output()
        

    def tweet_processing(self):
        inputfile = open(self.input, encoding='utf-8')
        data = inputfile.readlines()
        tweet_list = []
        tweet = []
        t = ""
        for line in data:
            line = line.split("\t")
            if line[0] != t:
                t = line[0]
                if tweet != []:
                    tweet_list.append(tweet)
                tweet = [line[4]]
            else:
                tweet.append(line[4])
        logging.info("List of Tweets generated")
        return tweet_list

    def ner(self): 
        stanza.download("es", processors={'ner': 'AnCora', 'tokenize': 'gsd'})
        #stanza.download("en")
        tweets = self.tweet_processing()
        nlp_en = stanza.Pipeline(lang='en', processors='tokenize,ner', tokenize_pretokenized=True)
        doc_en = nlp_en(tweets)
        results_en = []
        results_es = []
        for sentence in doc_en.sentences:
            for token in sentence.tokens:
                results_en.append([token.text, token.ner])
        nlp_es = stanza.Pipeline(lang='es', processors={'tokenize': 'gsd', 'ner': 'AnCora'}, tokenize_pretokenized=True)
        doc_es = nlp_es(tweets)
        for sentence in doc_es.sentences:
            for token in sentence.tokens:   
                #outputfile.write(token.text + '\t' + token.ner + '\n')
                results_es.append([token.ner])
                    
        logging.info("Outputfile generated")

        results = zip(results_en, results_es)

        return results
        

    def generate_output(self):
        #print(self.ner())
        outputfile = open(self.output, "w", encoding='utf-8')
        for result in self.ner():
            output = result[0] + result[1]
            outputfile.write("token: \t" + output[0] + "\tner english: \t" + output[1] + "\tner spanish: \t" + output[2] + "\n")


StanzaNER("data\EsEn_NER\calcs_dev.tsv", "outputs\output_stanza.txt")
